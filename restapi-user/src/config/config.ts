export const config = {
  "dev": {
    "username": process.env.POSTGRESS_USERNAME,
    "password": process.env.POSTGRESS_PASSWORD,
    "database": process.env.POSTGRESS_DB,
    "host": process.env.POSTGRESS_HOST,
    "dialect": "postgres",
    "aws_reigion": "us-west-2",
    "aws_profile": "default",
    "aws_media_bucket": process.env.AWS_BUCKET,
  },
  "prod": {
    "username": process.env.POSTGRESS_USERNAME,
    "password": process.env.POSTGRESS_PASSWORD,
    "database": process.env.POSTGRESS_DB,
    "host": process.env.POSTGRESS_HOST,
    "dialect": "postgres",
    "aws_reigion": "us-west-2",
    "aws_profile": "default",
    "aws_media_bucket": process.env.AWS_BUCKET,
  },
  "jwt": {
    "secret": process.env.JWT_SECRET
  }


}
